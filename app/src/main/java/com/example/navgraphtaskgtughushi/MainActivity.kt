package com.example.navgraphtaskgtughushi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

         val bottom_navigation_view :BottomNavigationView=findViewById(R.id.botton_navigation_view)
            val fragment_host_controller= findNavController(R.id.navhost)
        val appBarConfiguration = AppBarConfiguration(setOf(R.id.home_fragment,R.id.fragment_subscriptions,R.id.fragment_shorts,R.id.fragment_library))
        setupActionBarWithNavController(fragment_host_controller,appBarConfiguration)
        bottom_navigation_view.setupWithNavController(fragment_host_controller)
    }
}