package com.example.navgraphtaskgtughushi.Fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.navgraphtaskgtughushi.R

class home_fragment : Fragment(R.layout.home_fragment_layout) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
         val inputfield =view.findViewById<EditText>(R.id.inputtext)
         val passbtn =view.findViewById<Button>(R.id.passdatabtn)

                val controller = Navigation.findNavController(view)

             passbtn.setOnClickListener {
                 var inputedtext=inputfield.text.toString()
                 if(!inputedtext.isEmpty()){

                     val action =home_fragmentDirections.actionHomeFragmentToFragmentShorts(inputedtext)

                        controller.navigate(action)

                 }

             }

    }

}